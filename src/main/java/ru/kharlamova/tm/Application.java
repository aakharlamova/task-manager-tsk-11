package ru.kharlamova.tm;

import ru.kharlamova.tm.bootstrap.Bootstrap;

import java.util.UUID;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
