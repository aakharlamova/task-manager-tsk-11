package ru.kharlamova.tm.api;

import ru.kharlamova.tm.model.Project;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProject(Project project);

    void showProjectByName();

    void showProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeProjectById();

    void  updateProjectByIndex();

    void  updateProjectById();

}
