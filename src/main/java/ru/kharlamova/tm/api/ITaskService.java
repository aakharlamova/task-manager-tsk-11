package ru.kharlamova.tm.api;

import ru.kharlamova.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByName(String name);

    Task removeOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

}
